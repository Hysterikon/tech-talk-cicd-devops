image: lorisleiva/laravel-docker

stages:
  - dockerbuild
  - preparation
  - testing
  - security
  - deploy

cache:
  key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
  paths:
    - vendor
    - node_modules
    - public
    - .yarn

# Docker Build (Build Production Environment)
build-image:
  stage: dockerbuild
  allow_failure: true #Debug Mode: Disable if this should be pipeline breaking
  script:
    - docker-compose build --no-cache --compress --parallel

# Preparation Stage
composer:
  stage: preparation
  script:
    - php -v
    - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
  artifacts:
    paths:
      - vendor/
      - .env
    expire_in: 1 days
    when: always

yarn:
  stage: preparation
  allow_failure: false # Debug Mode: set to true if yarn is not needed
  script:
    - yarn --version
    - yarn install --pure-lockfile
  artifacts:
    paths:
      - node_modules
    expire_in: 1 days
    when: always

# Testing Stage
phpunit:
  stage: testing
  dependencies:
    - composer
  script:
    - php -v
    - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
    - cp .env.example .env
    - php artisan key:generate
    - phpunit --version
    - phpunit --coverage-text --colors=never
  artifacts:
    paths:
      - ./storage/logs
    expire_in: 1 days
    when: on_failure

codestyle:
  stage: testing
  script:
    - phpcs --extensions=php app --colors -n --standard=PSR12
    - phpcbf -n --standard=PSR12 --extensions=php app
  dependencies:
    - composer
    - yarn

phpcpd:
  stage: testing
  script:
    - test -f phpcpd.phar || curl -L https://phar.phpunit.de/phpcpd.phar -o phpcpd.phar
    - php phpcpd.phar app/ --min-lines=50
  dependencies:
    - composer
    - yarn

sonarqube:
  image:
    name: sonarsource/sonar-scanner-cli:latest
  variables:
    SONAR_TOKEN: "f4b9e20087979875780abd0d57c5f05f25e54182"
    SONAR_HOST_URL: "http://sonarqube.aptinstall.de"
    SONAR_USER_HOME: $CI_PROJECT_DIR"/.sonar" #Defines the location of the analysis task cache
    GIT_DEPTH: 0 #Tells git to fetch all the branches of the project, required by the analysis task
  stage: testing
  script:
    - cd ./application
    - sonar-scanner -Dsonar.projectKey=laravel-boilerplate -Dsonar.login=f4b9e20087979875780abd0d57c5f05f25e54182
  only:
    - develop
    - master
    - release

#Security Stage
enlightn:
  stage: security
  script:
    - composer require enlightn/enlightn
    - php artisan vendor:publish --tag=enlightn
    # - mkdir ./reports
    - php artisan enlightn --ci
    # - php artisan enlightn --ci >> ./reports/enlightn_report.txt
  dependencies:
    - composer
    - yarn
  cache:
    paths:
      - security-checker/
  artifacts:
    paths:
      - ./reports/enlightn_report.txt
    expire_in: 1 days
    when: always
  only:
    - develop
    - master
    - release

# Deploy DEV
deploy-dev:
 stage: deploy
 dependencies:
   - composer
   - yarn
 script:
   - composer require --dev clue/phar-composer
   - ./vendor/bin/phar-composer --version
   - echo "Starting deploy ..."
   - phar-composer build ./
   - scp ./laravel.phar username@a:/path/to/destination
   - echo "Done ...."
 only:
   - develop

# Deploy UAT
deploy-uat:
  stage: deploy
  dependencies:
    - composer
    - yarn
  script:
   - composer require --dev clue/phar-composer
   - ./vendor/bin/phar-composer --version
   - echo "Starting deploy ..."
   - phar-composer build ./
   - scp ./laravel.phar username@a:/path/to/destination
   - echo "Done ...."
  only:
    - master

# Deploy PROD
deploy-prod:
  stage: deploy
  dependencies:
    - composer
    - yarn
  script:
   - composer require --dev clue/phar-composer
   - ./vendor/bin/phar-composer --version
   - echo "Starting deploy ..."
   - phar-composer build ./
   - scp ./laravel.phar username@a:/path/to/destination
   - echo "Done ...."
  only:
    - release
