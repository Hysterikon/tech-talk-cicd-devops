# Handout: DevOps CICD

Levin Hennig und Marc Hannappel



## 1. Was ist eigentlich DevOps ?

DevOps beschreibt die enge zusammenarbeit zwischen Entwicklern und dem IT-Betrieb eines Unternehmens. Durch das leben von DevOps werden Prozesse der Softwareentwicklung effizienter und effektiver.



## 2. Was ist CICD ?

CICD (Continuous Integration und Continuous Deployment) ist ein Ansatz bei dem die Automatisierung von Entwicklungsprozessen im Vordergrund steht. Hierbei soll durch das entfernen der Fehlerquelle Mensch eine höhere Qualität und Effizienz des Endprodukts erreicht werden.

Typischerweise automatisiert man Entwicklungsprozesse von Versions Kontrolle (VSC) bis Bereitstellung (Deployment) da so der ganze Lifecycle während der Entwicklung abgedeckt wird.

Folgende Prozesse werden somit automatisiert:

- Feature Planung + Releaseplanung
- Versionskontrolle / Source Code Management
- Dependency Management (bsp. NPM oder Maven)
- Kompilieren / Transpilieren
- Testing / QA
- Security
- Deployment
- Hosting + Wartung + Betrieb

Durch dieses Vorgehen wird viel Zeit gespart die während des Entwicklungsverlauf einer Applikation dann anderweitig verplant werden kann. Praktisch ist hierbei das bei korrekter Anwendung von CICD eine hohe wieder Verwendbarkeit der Prozesse garantiert ist.



## 3. Wie wird CICD in der Praxis angewendet ?

Um CICD in der Praxis anzuwenden werden häufig sogenannte CICD Pipelines entwickelt. Diese bestehen aus Script Code welcher die nötigen Prozesse abbildet. Typischerweise werden Pipelines mithilfe eines Tools ausgeführt (z.B Bamboo oder GitLab). Durch den Faktor das Pipelines als Code entwickelt werden können diese ebenfalls Versioniert werden und erreichen so eine hohe Wiederverwendbarkeit in ähnlichen Projekten und können als Vorlage für andere Projekte dienen.

![Beispiel einer CICD Pipeline in GitLab](C:\Users\thede\Desktop\pipeline-example.png)



## 4. Welcher Toolstack ist der richtige ?

Da es für die Automatisierung von Entwicklungsprozessen heutzutage eine Vielzahl an Tools gibt ist es hilfreich sich auf einen bestimmten Toolstack festzulegen der die Bedürfnisse der Applikation oder des angestrebten Firmenstandards bedient.

Um die richtige Wahl treffen zu können macht es Sinn sich die folgenden Fragen zu stellen:

1. Versioniere ich meinen Code ? Wenn ja mit welcher Plattform ?
2. Welche Programmiersprache wird im Projekt verwendet ?
3. Welches Dependency Management Tool benutze ich ?
   - Wie installiere ich Dependencies ?
   - Wie checke ich Depenencies auf Sicherheitslücken ?
4. Wird mein Code Kompiliert, Transpiliert oder einfach nur ausgeführt ?
5. Benutze ich ein Testing Framework ? Wenn ja welches ?
   - Wie führe ich Tests aus ?
6. Wie prüfe ich meine Applikation auf Sicherheitslücken ?
7. Wird meine Applikation auf Servern bereitgestellt ?
   - Welches Betriebssystem läuft auf den Servern ?
   - Welche Software wird benötigt um die Applikation auf den Servern in Betrieb zu nehmen ?
   - Welche Server werden mit der Applikation bespielt ?
   - Wie regele ich Server Last ?
   - Wie manage ich Applikationsausfälle ?

Mithilfe dieser Liste kann das große Angebot der Tools auf wenige eingegrenzt werden.



```yaml
# CICD Pipeline für ein Laravel Projekt (Code aus der Live Coding Session)

image: lorisleiva/laravel-docker

stages:
  - dockerbuild
  - preparation
  - testing
  - security
  - deploy

cache:
  key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
  paths:
    - vendor
    - node_modules
    - public
    - .yarn

# Docker Build (Build Production Environment)
build-image:
  stage: dockerbuild
  allow_failure: true #Debug Mode: Disable if this should be pipeline breaking
  script:
    - docker-compose build --no-cache --compress --parallel

# Preparation Stage
composer:
  stage: preparation
  script:
    - php -v
    - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
  artifacts:
    paths:
      - vendor/
      - .env
    expire_in: 1 days
    when: always

yarn:
  stage: preparation
  allow_failure: false # Debug Mode: set to true if yarn is not needed
  script:
    - yarn --version
    - yarn install --pure-lockfile
  artifacts:
    paths:
      - node_modules
    expire_in: 1 days
    when: always

# Testing Stage
phpunit:
  stage: testing
  dependencies:
    - composer
  script:
    - php -v
    - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
    - cp .env.example .env
    - php artisan key:generate
    - phpunit --version
    - phpunit --coverage-text --colors=never
  artifacts:
    paths:
      - ./storage/logs
    expire_in: 1 days
    when: on_failure

codestyle:
  stage: testing
  script:
    - phpcs --extensions=php app --colors -n --standard=PSR12
    - phpcbf -n --standard=PSR12 --extensions=php app
  dependencies:
    - composer
    - yarn

phpcpd:
  stage: testing
  script:
    - test -f phpcpd.phar || curl -L https://phar.phpunit.de/phpcpd.phar -o phpcpd.phar
    - php phpcpd.phar app/ --min-lines=50
  dependencies:
    - composer
    - yarn

sonarqube:
  image:
    name: sonarsource/sonar-scanner-cli:latest
  variables:
    SONAR_TOKEN: "token"
    SONAR_HOST_URL: "http://sonarqube.servername.de"
    SONAR_USER_HOME: $CI_PROJECT_DIR"/.sonar" #Defines the location of the analysis task cache
    GIT_DEPTH: 0 #Tells git to fetch all the branches of the project, required by the analysis task
  stage: testing
  script:
    - cd ./application
    - sonar-scanner -Dsonar.projectKey=laravel-boilerplate -Dsonar.login=token
  only:
    - develop
    - master
    - release

#Security Stage
enlightn:
  stage: security
  script:
    - composer require enlightn/enlightn
    - php artisan vendor:publish --tag=enlightn
    # - mkdir ./reports
    - php artisan enlightn --ci
    # - php artisan enlightn --ci >> ./reports/enlightn_report.txt
  dependencies:
    - composer
    - yarn
  cache:
    paths:
      - security-checker/
  artifacts:
    paths:
      - ./reports/enlightn_report.txt
    expire_in: 1 days
    when: always
  only:
    - develop
    - master
    - release

# Deploy DEV
deploy-dev:
 stage: deploy
 dependencies:
   - composer
   - yarn
 script:
   - composer require --dev clue/phar-composer
   - ./vendor/bin/phar-composer --version
   - echo "Starting deploy ..."
   - phar-composer build ./
   - scp ./laravel.phar username@a:/path/to/destination
   - echo "Done ...."
 only:
   - develop

# Deploy UAT
deploy-uat:
  stage: deploy
  dependencies:
    - composer
    - yarn
  script:
   - composer require --dev clue/phar-composer
   - ./vendor/bin/phar-composer --version
   - echo "Starting deploy ..."
   - phar-composer build ./
   - scp ./laravel.phar username@a:/path/to/destination
   - echo "Done ...."
  only:
    - master

# Deploy PROD
deploy-prod:
  stage: deploy
  dependencies:
    - composer
    - yarn
  script:
   - composer require --dev clue/phar-composer
   - ./vendor/bin/phar-composer --version
   - echo "Starting deploy ..."
   - phar-composer build ./
   - scp ./laravel.phar username@a:/path/to/destination
   - echo "Done ...."
  only:
    - release
```



## 5. Aufgaben



1. Wählen sie eine Applikation (real existierend oder Wunsch Projekt) und beantworten sie die Fragen aus Abschnitt 4 für diese Applikation.
2. Wählen sie mithilfe der Antworten aus Aufgabe 1 ihre Werkzeuge aus.
3. Erstellen sie mit den gelernten Informationen und ihren Antworten aus Aufgabe 1 und 2 eine Beispielhafte GitLab CICD Pipeline. Tipp: Falls ihnen Befehle fehlen sollten schauen sie sich die [offizielle Dokumentation](https://docs.gitlab.com/ee/ci/README.html) von GitLab und das Code Beispiel aus Abschnitt 4 näher an.



## 6. Weitere interessante Themen

- [Gitflow](https://www.atlassian.com/de/git/tutorials/comparing-workflows/gitflow-workflow) für eine bessere Versionskontrolle
- Docker Images für CICD und Deployments



## Quellen

- [Wikipedia DevOps](https://de.wikipedia.org/wiki/DevOps)
- [Atlassian CICD](
